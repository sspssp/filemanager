from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect

def helloworld(request):
	return render_to_response('home.html')
    #return HttpResponse('hello world')

def loginView(request):
	if request.method == 'POST':
		user = authenticate(username=request.POST["user"], password=request.POST["pass"])
		if user is not None:
			if user.is_active:
				login(request, user)
				try:
					return redirect(request.GET["next"])
				except:
					pass
				return HttpResponse("Login erfolgreich")
	try:
		next = request.GET["next"]
	except:
		next = "/documents/"

	return render_to_response("login.html", {"next" : next})

def register(request):
	return render_to_response("content/reg.html")

def funktionen(request):
	return render_to_response("content/funktionen.html")
	

