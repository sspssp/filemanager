from django.conf.urls import patterns, include, url
import fileManager.views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', fileManager.views.helloworld),
	url(r'^accounts/login', fileManager.views.loginView),
    url(r'^accounts/register', fileManager.views.register),
    url(r'^functions', fileManager.views.funktionen),
    # Examples:
    # url(r'^$', 'fileManager.views.home', name='home'),
    # url(r'^fileManager/', include('fileManager.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),
    #url(r'^admin/', include('django.contrib.admin.urls')),

    (r'^documents/', include('fileManager.dokument.urls')),
)
