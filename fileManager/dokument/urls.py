from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url(r'^$', views.uebersicht),
    url(r'^new$', views.upload),
    url(r'^tag/add$', views.addTag),
    url(r'^tag/del$', views.delTag),
    url(r'^tags$', views.viewTag),
    url(r'^notiz/add$', views.addNotiz),
    url(r'^status/change$', views.statusChange),
    url(r'^status$', views.viewStatus),
    url(r'^search$', views.search),
    url(r'^download$', views.download),
    url(r'^(?P<id>\d{1,9})$', views.details),

)