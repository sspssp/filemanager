from django import template
from fileManager.dokument.models import *
import operator

register = template.Library()

def createMenu(typ): # Only one argument.
	"""Converts a string into all lowercase"""
	menuString = ""
	#print typ
	if(typ=="tag"):
		tags = getTopKeyWords()
		try:
			menuString = menuString + '<li><a href="/documents/tags?tag='+tags[0][1]+'"><span class="email-label-personal"></span>'+tags[0][1]+'</a></li>'
		except:
			pass
		try:
			menuString = menuString + '<li><a href="/documents/tags?tag='+tags[1][1]+'"><span class="email-label-work"></span>'+tags[1][1]+'</a></li>'
		except:
			pass
		try:
			menuString = menuString + '<li><a href="/documents/tags?tag='+tags[2][1]+'"><span class="email-label-travel"></span>'+tags[2][1]+'</a></li>'
		except:
			pass
	if(typ=="status"):
		status = Status.objects.all()
		for s in status:
			menuString = menuString + '<li><a href="/documents/status?status='+s.name+'">'+s.name+'</a></li>'
	return menuString

def getTopKeyWords():
	tags = Tag.objects.all()
	topTags = list()
	for tag in tags:
		dokumente = Dokument.objects.filter(tag__name=tag.name).count()
		#print tag.name+": "+str(dokumente)
		topTags.append([dokumente, tag.name])
	#sorted(topTags, key=operator.itemgetter(1))
	topTags.sort(reverse=True)
	i = 0
	returnTags = list()
	for tag in topTags:
		if i < 3:
			returnTags.append(tag)
		i = i + 1
	return returnTags
register.filter('createMenu', createMenu)