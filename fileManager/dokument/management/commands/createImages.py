from django.core.management.base import BaseCommand, CommandError
#from polls.models import Poll
from fileManager.dokument.models import Dokument
import os
import subprocess

class Command(BaseCommand):
    args = '[<id>|all|new]'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for image in args:
            if(image == "all"):
                dokumente = Dokument.objects.all()
                for d in dokumente:
                    createImg(d)
            elif(image == "new"):
                dokumente = Dokument.objects.all()
                for d in dokumente:
                    if(d.imgURL == "/static/img/warten.gif"):
                        createImg(d)
            elif(image == "del"):
                dokumente = Dokument.objects.all()
                for d in dokumente:
                    d.imgURL = "/static/img/warten.gif"
                    d.save()
            else:
                dokumente = Dokument.objects.get(pk=image)
                createImg(dokumente)
        print args


def createImg(d):
    if(d.mimeTyp == "application/pdf"):
        print "Create Impage %s" % str(d)
        order = 'convert -density 600x600 -resize 800x560 "'+str(d.path)+'"[0] "'+str(os.path.dirname(__file__))+'/../../../static/img/create/'+str(d.id)+'.png"'
        print order
        convert = subprocess.Popen(order, shell=True) #os.system
        #subprocess.Popen.wait(convert)
        d.imgURL = '/static/img/create/'+str(d.id)+'.png'
        d.save()
    else:
        print "Kein Gueltiger MimeTyp"
        d.imgURL = '/static/img/unbkeannt.jpg'
        d.save()