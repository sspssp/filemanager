# -*- coding: utf_8 -*-
from django.core.management.base import BaseCommand, CommandError
#from polls.models import Poll
from fileManager.dokument.models import Dokument
import os
import subprocess

class Command(BaseCommand):
    args = '[<id>|all|new]'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for arg in args:
            if arg == "new":
                pass
            elif arg == "all":
                pass
            else:

                d = Dokument.objects.get(pk=arg)
                read(d)
                #pdftoppm d.path | ocrad 

def read(d):
    if(d.mimeTyp == "application/pdf"):
        output = subprocess.Popen("pdftotext "+d.path+" "+d.path+".txt", stdout=subprocess.PIPE, shell=True).communicate()[0]
        #pdftotext ~/Downloads/Schiedsgericht.pdf  test.txt
        text = ""
        print d.path+".txt"
        fobj = open(d.path+".txt")
        for line in fobj:
            print line.decode('ascii', 'ignore') #.encode('ascii', 'ignore')
            #text = unicode(line, 'ascii')
            text = text + line.decode('ascii', 'ignore')
        fobj.close()
        #print text
        d.text = text
        d.save()
    else:
        print "Falscher Mine Typ"

