# -*- coding: utf_8 -*-
from django.core.management.base import BaseCommand, CommandError
#from polls.models import Poll
from fileManager.dokument.models import Dokument
import os
import subprocess

class Command(BaseCommand):
    args = '[<id>|all|new]'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for arg in args:
            if arg == "new":
                pass
            elif arg == "all":
                pass
            else:
                d = Dokument.objects.get(pk=arg)
                scan(d)
                #pdftoppm d.path | ocrad 

def scan(d):
    if(d.mimeTyp == "application/pdf"):
        output = subprocess.Popen("pdftoppm "+d.path+" | ocrad -c ascii", stdout=subprocess.PIPE, shell=True).communicate()[0]
        d.ocr = str(output).encode("iso-8859-1")
        d.save()