from django.contrib import admin
from models import *

admin.site.register(Dokument)
admin.site.register(Notiz)
admin.site.register(Tag)
admin.site.register(Status)