from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

class Status(models.Model):
	name = models.CharField(max_length=255)
	def __str__(self):
		return self.name

class Dokument(models.Model):
	user = models.ForeignKey(User)
	status = models.ForeignKey(Status)
	titel = models.CharField(max_length=100)
	ocr = models.TextField()
	text = models.TextField()
	suchText = models.TextField()
	create_date = models.DateTimeField(default=datetime.now)
	path = models.CharField(max_length=255)
	imgURL  = models.CharField(max_length=255, default="/static/img/warten.gif")
	mimeTyp = models.CharField(max_length=255)
	fileName = models.CharField(max_length=255)
	def __str__(self):
		return self.titel.encode('ascii', 'ignore')

class Notiz(models.Model):
	document = models.ForeignKey(Dokument)
	text = models.TextField()
	create_date = models.DateTimeField(default=datetime.now)
	def __str__(self):
		return self.text

class Tag(models.Model):
	document = models.ManyToManyField(Dokument)
	name = models.CharField(max_length=255)
	def __str__(self):
		return self.name
