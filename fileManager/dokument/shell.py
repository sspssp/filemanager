import subprocess

class shell(object):
	def scanPDF(d):
    if(d.mimeTyp == "application/pdf"):
        output = subprocess.Popen("pdftoppm "+d.path+" | ocrad -c ascii", stdout=subprocess.PIPE, shell=True).communicate()[0]
        d.ocr = str(output).encode("iso-8859-1")
        d.save()