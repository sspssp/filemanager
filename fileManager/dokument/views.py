from django.contrib.auth.decorators import login_required
from models import *
from django.shortcuts import render_to_response
from django.http import HttpResponse
import os
import subprocess
from subprocess import call
import time
import os.path
from django.utils.translation import ugettext
import re
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
import magic
from django.db.models import Q
from django.core.servers.basehttp import FileWrapper
#from pyPdf import PdfFileReader
#from PythonMagick import Image
#from .forms import UploadFileForm

@login_required
def uebersicht(request):
	#createImages()
	try:
		currentPage = int(request.GET["page"])
	except:
		currentPage = 1
	try:
		start = (currentPage - 1) * 20
		ende = start + 20
	except:
		start = 0
		ende = 20
	countDokumente = Dokument.objects.filter(user = request.user).count()
	pages = countDokumente / 20;
	pageViewer = list()
	if(currentPage>3):
		pageViewer.append(currentPage-2)
		pageViewer.append(currentPage-1)
		pageViewer.append(currentPage)
		pageViewer.append(currentPage+1)
		pageViewer.append(currentPage+2)
	else:
		if(pages>=0):
			pageViewer.append(1)
		if(pages>=1):
			pageViewer.append(2)
		if(pages>=2):
			pageViewer.append(3)
		if(pages>=3):
			pageViewer.append(4)
		if(pages>=4):
			pageViewer.append(5)

	dokumente = Dokument.objects.filter(user = request.user).order_by('-create_date')[start:ende]
	return render_to_response("dokumente/view.html", {'dokumente': dokumente, 'pageViewer':pageViewer})

@login_required
def details(request, id=1):
	dokumente = Dokument.objects.get(pk=id, user = request.user)
	tags = dokumente.tag_set.all()
	notizen = dokumente.notiz_set.all().order_by('-create_date')[:20]
	statuse = Status.objects.all()
	alltags = Tag.objects.all()
	return render_to_response("dokumente/details.html", {'dokumente': dokumente, 'tags': tags, 'alltags':alltags, 'notizen':notizen, 'statuse': statuse})

@login_required
def upload(request):
	if request.method == 'POST':
		d = Dokument()
		d.user = request.user
		d.titel = request.POST["text"]
		d.ocr = ""
		d.path = ""
		d.imgURL = "/static/img/warten.gif"
		d.status = Status.objects.get(name=request.POST["status"])
		d.save()
		handle_uploaded_file(request.FILES['dokument'], d)
		return redirect('/documents/'+str(d.id))
		return HttpResponse('Datei erfolgreich hochgeladen')
	else:
		stati = Status.objects.all()
		return render_to_response("dokumente/new.html", {"status":stati})

def handle_uploaded_file(f, d):
	with open(str("files/"+str(d.id)+"_"+str(f)), 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)
		d.path=os.path.dirname(__file__)+"/../../files/"+str(d.id)+"_"+str(f)
		d.fileName = str(f)
		mime = magic.Magic(mime=True)
		d.mimeTyp = mime.from_file(d.path)
		d.save()

@login_required
def addTag(request):
	# Search if Tag exist
	regex = re.compile("[a-zA-Z0-9]")
	zeichen = regex.findall(request.GET["tag"])
	tagName = ""
	for z in zeichen:
		tagName = tagName + z
	tags = Tag.objects.filter(name = tagName)
	if tags.count() == 0:
		tag = Tag()
		tag.name = tagName
		print tag.name
		tag.save()
		tag.document.add(Dokument.objects.get(pk=request.GET["documentID"]))
		tag.save()
	elif tags.count() == 1:
		tag = tags[0]
		tag.document.add(Dokument.objects.get(pk=request.GET["documentID"]))
		tag.save()
	else:
		return HttpResponse(status=500)
	return HttpResponse('Tag ADD')


@login_required
def delTag(request):
	regex = re.compile("[a-zA-Z0-9]")
	zeichen = regex.findall(request.GET["tag"])
	tagName = ""
	for z in zeichen:
		tagName = tagName + z
	tags = Tag.objects.filter(name = tagName)
	dokumente = Dokument.objects.get(pk=request.GET["documentID"])
	tags[0].document.remove(dokumente)
	return HttpResponse('Tag DEL')


@login_required
def viewTag(request):
	try:
		currentPage = int(request.GET["page"])
	except:
		currentPage = 1
	try:
		start = (currentPage - 1) * 20
		ende = start + 20
	except:
		start = 0
		ende = 20
	countDokumente = Dokument.objects.filter(user = request.user, tag__name=request.GET["tag"]).count()
	pages = countDokumente / 20;
	pageViewer = list()
	if(currentPage>3):
		pageViewer.append(currentPage-2)
		pageViewer.append(currentPage-1)
		pageViewer.append(currentPage)
		pageViewer.append(currentPage+1)
		pageViewer.append(currentPage+2)
	else:
		if(pages>=0):
			pageViewer.append(1)
		if(pages>=1):
			pageViewer.append(2)
		if(pages>=2):
			pageViewer.append(3)
		if(pages>=3):
			pageViewer.append(4)
		if(pages>=4):
			pageViewer.append(5)
	#createImages()
	dokumente = Dokument.objects.filter(user = request.user, tag__name=request.GET["tag"]).order_by('-create_date')[:20]
	return render_to_response("dokumente/view.html", {'dokumente': dokumente, 'pageViewer':pageViewer})

@login_required
def viewStatus(request):
	try:
		currentPage = int(request.GET["page"])
	except:
		currentPage = 1
	try:
		start = (currentPage - 1) * 20
		ende = start + 20
	except:
		start = 0
		ende = 20
	countDokumente = Dokument.objects.filter(user = request.user).count()
	pages = countDokumente / 20;
	pageViewer = list()
	if(currentPage>3):
		pageViewer.append(currentPage-2)
		pageViewer.append(currentPage-1)
		pageViewer.append(currentPage)
		pageViewer.append(currentPage+1)
		pageViewer.append(currentPage+2)
	else:
		if(pages>=0):
			pageViewer.append(1)
		if(pages>=1):
			pageViewer.append(2)
		if(pages>=2):
			pageViewer.append(3)
		if(pages>=3):
			pageViewer.append(4)
		if(pages>=4):
			pageViewer.append(5)
	#createImages()
	dokumente = Dokument.objects.filter(user = request.user, status__name=request.GET["status"]).order_by('-create_date')[:20]
	return render_to_response("dokumente/view.html", {'dokumente': dokumente, 'pageViewer':pageViewer})

@login_required
def addNotiz(request):
	notiz = Notiz()
	notiz.text = request.POST["text"]
	d = Dokument.objects.get(pk=request.GET["documentID"])
	notiz.document = d
	notiz.save()
	return redirect('/documents/'+str(d.id))

@login_required
def statusChange(request):
	dokument = Dokument.objects.get(pk=request.GET["documentID"]);
	status = Status.objects.get(pk=request.GET["statusID"])
	dokument.status = status
	dokument.save()
	return HttpResponse(str(dokument.status))

@login_required
def search(request):
	if request.method == 'POST':
		volltext = 0
		try:
			request.POST["volltext"]
			volltext = 1
			dokumente = Dokument.objects.filter(Q(titel__icontains=request.POST["suchwort"]) | Q(tag__name__icontains=request.POST["suchwort"])| Q(notiz__text__icontains=request.POST["suchwort"])| Q(text__icontains=request.POST["suchwort"])| Q(ocr__icontains=request.POST["suchwort"]))
		except:
			dokumente = Dokument.objects.filter(Q(titel__icontains=request.POST["suchwort"]) | Q(tag__name__icontains=request.POST["suchwort"])| Q(notiz__text__icontains=request.POST["suchwort"]))
		try:
			currentPage = int(request.GET["page"])
		except:
			currentPage = 1
		try:
			start = (currentPage - 1) * 20
			ende = start + 20
		except:
			start = 0
			ende = 20
		countDokumente = dokumente.count()
		pages = countDokumente / 20;
		pageViewer = list()
		if(currentPage>3):
			pageViewer.append(currentPage-2)
			pageViewer.append(currentPage-1)
			pageViewer.append(currentPage)
			pageViewer.append(currentPage+1)
			pageViewer.append(currentPage+2)
		else:
			if(pages>=0):
				pageViewer.append(1)
			if(pages>=1):
				pageViewer.append(2)
			if(pages>=2):
				pageViewer.append(3)
			if(pages>=3):
				pageViewer.append(4)
			if(pages>=4):
				pageViewer.append(5)
		dokumente = dokumente[start:ende]
		return render_to_response("dokumente/search.html", {'dokumente': dokumente, 'suchwort':request.POST["suchwort"], "volltext":volltext, 'pageViewer':pageViewer})
	else:
		return render_to_response("dokumente/search.html")

@login_required
def download(request):
	d = Dokument.objects.get(pk=request.GET["documentID"], user = request.user)
	fileContent = ""
	fobj = open(d.path, "r")
	for line in fobj:
		fileContent = fileContent + line
	fobj.close()

	response = HttpResponse(fileContent, content_type=d.mimeTyp)
	response['Content-Disposition'] = 'attachment; filename=%s' % (d.fileName)
	return response